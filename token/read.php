<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

include_once '../config/database.php';
include_once '../objects/token.php';

$database = new Database();
$db = $database->getConnection();
 
$token = new Token($db);

$stmt = $token->read();
$num = $stmt->rowCount();

if($num>0){
    $tokens_arr=array();
    $tokens_arr["records"]=array();
	
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        extract($row);
 
        $token_item=array(
            "id" => $ID,
            "login_utc" => $login_utc,
            "id_app" => $id_app,
        );
 
        array_push($tokens_arr["records"], $token_item);
    }
 
    http_response_code(200);
	
    echo json_encode($tokens_arr);
}else{
    http_response_code(404);
 
    echo json_encode(
        array("message" => "No tokens found.")
    );
}