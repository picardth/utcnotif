<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

include_once '../config/database.php';

include_once '../objects/token.php';
 
$database = new Database();
$db = $database->getConnection();
 
$token = new token($db);

$data = json_decode(file_get_contents("php://input"));

if(
    !empty($data->login_utc) &&
    !empty($data->id_app)
){
    $token->login_utc = $data->login_utc;
    $token->id_app = $data->id_app;

    if($token->create()){
        http_response_code(201);

        echo json_encode(array("message" => "Token was created."));
    }else{
        http_response_code(503);

        echo json_encode(array("message" => "Unable to create token."));
    }
}else{
    http_response_code(400);

    echo json_encode(array("message" => "Unable to create token. Data is incomplete."));
}
?>