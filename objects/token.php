<?php
class Token{
 
    private $conn;
    private $table_name = "logintoken";
 
    public $id;
    public $login_utc;
    public $id_app;
 
    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }
	
	function read(){
		$query = "SELECT * FROM " . $this->table_name . ";";
		$stmt = $this->conn->prepare($query);
		$stmt->execute();
	 
		return $stmt;
	}
	
	function create(){
		$query = "INSERT INTO
                " . $this->table_name . "
            SET
                login_utc=:login_utc, id_app=:id_app";
	 
		$stmt = $this->conn->prepare($query);
	
		$stmt->bindParam(":login_utc", $this->login_utc);
		$stmt->bindParam(":id_app", $this->id_app);
	 
		if($stmt->execute()){
			return true;
		}
		return false;
	}
	
	function readOne(){
	 	$query = "SELECT ID, login_utc, id_app FROM " . $this->table_name . " WHERE login_utc=:login_utc;";
		$stmt = $this->conn->prepare( $query );
		$stmt->bindParam(":login_utc", $this->login_utc);
		$stmt->execute();
		return $stmt;
	}
}
?>